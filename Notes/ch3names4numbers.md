# Chapter 3: Names for Numbers

Oren will put notes here.
A computer can associate a name with a value
Variable: Space in computer memory that can represent a value
You can associate a name with a value
A computer’s memory is made up of numbers
Setting a variable’s value is called assignment
Variable names must start with a letter or an underscore, can’t be a python keyword, and case does matter (result vs Result), cannot have spaces
Capitalizing the first letter of each new word is called camel-case or mixed-case
Arithmetic Expression - An arithmetic expression contains a mathematical operator like - for subtraction or * for multiplication
Assignment means setting a variable’s value. For example, x = 5 assigns the value of 5 to a variable called x
Assignment Dyslexia is putting the variable name on the right and the value on the left as in 5 = x. This is not a legal statement.
Integer division is when you divide one integer by another. In some languages this will only give you an integer result, but in Python 3 it returns a decimal value, unless you use the floor division operator, //
The modulo, or remainder operator, %, returns the remainder after you divide one value by another. For example the remainder of 3 % 2 is 1 since 2 goes into 3 one time with a remainder of 1
Tracing a program means keeping track of the variables in the program and how their values change as the statements are executed. We used a Code Lens tool to do this in this chapter
A variable is a name associated with computer memory that can hold a value and that value can change or vary. One example of a variable is the score in a game

